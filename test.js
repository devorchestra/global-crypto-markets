const Markets = require('./build/index').default;

let markets = new Markets();
markets.init();
setInterval(()=>{
    console.log(markets.getExchangePrice('BTC',"EUR"));
    console.log(markets.getExchangePrice('ETH',"EUR"));
    console.log(markets.getExchangePrice('ETH',"USD"));
    console.log(markets.getExchangePrice('ETH',"ETH"));
    console.log(markets.getExchangePrice('USD',"USD"));
    console.log(markets.getExchangePrice('EUR',"EUR"));
    console.log(markets.getExchangePrice('TMU',"EUR"));
    console.log(markets.getExchangePrice('OPEX',"USD"));
    console.log(markets.getExchangePrice('COI',"EUR"));
},1000);