module.exports = {
    out: './docs/',
    exclude: [
        '**/tests/**/*',
        '**/pairs.ts',
        '**/logger.ts',
    ]
};