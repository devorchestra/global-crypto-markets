module.exports = [
    ["BTCUSD", { "name": "BTC/USD" }],
    ["BTCEUR", { "name": "BTC/EUR" }],
    ["LTCUSD", { "name": "LTC/USD" }],
    ["LTCBTC", { "name": "LTC/BTC" }],
    ["ETHUSD", { "name": "ETH/USD" }],
    ["ETHBTC", { "name": "ETH/BTC" }],
    ["DSHUSD", { "name": "DASH/USD" }],
    ["DSHBTC", { "name": "DASH/BTC" }],
    //["DSHETH", {"name": "DASH/ETH"}], no such pair
    ["EOSUSD", { "name": "EOS/USD" }],
    ["EOSBTC", { "name": "EOS/BTC" }],
    ["EOSETH", { "name": "EOS/ETH" }],
    ["TRXUSD", { "name": "TRX/USD" }],
    ["TRXBTC", { "name": "TRX/BTC" }],
    ["TRXETH", { "name": "TRX/ETH" }],
    ["OMGUSD", { "name": "OMG/USD" }],
    ["OMGBTC", { "name": "OMG/BTC" }],
    ["OMGETH", { "name": "OMG/ETH" }],
    ["SNTUSD", { "name": "SNT/USD" }],
    ["SNTBTC", { "name": "SNT/BTC" }],
    ["SNTETH", { "name": "SNT/ETH" }],
];
