"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
const parseString = require('xml2js').parseString;
/**
 * @hidden
 */
const rp = require('request-promise');
/**
 * This abstract class is responsible for describing
 * global cryptocurrency market that supports XML API.
 */
const market_1 = require("./market");
class MarketViaXML extends market_1.default {
    constructor(config) {
        super();
        this.config = config;
    }
    getData() {
        return rp(this.url)
            .then(string => {
            return string;
            // return new Promise((resolve, reject) => {
            //     parseString(string, {trim: true}, (err, result) => {
            //         if (err) {
            //             reject(err);
            //         } else {
            //             resolve(result);
            //         }
            //     });
            // });
        });
    }
}
exports.default = MarketViaXML;
