"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
const WebSocket = require("ws");
/**
 * @hidden
 */
const logger_1 = require("../logger");
/**
 * @hidden
 */
const async = require("async");
/**
 * @hidden
 */
const _ = require("lodash");
const market_1 = require("./market");
/**
 * This abstract class is responsible for describing
 * global cryptocurrency market that supports WebSocket API.
 */
class MarketViaWebSocket extends market_1.default {
    /**
     * @param config parsed config object
     */
    constructor(config) {
        super(config);
        this.config = config;
        this.isRestartScheduled = false;
        this.isAlive = true;
        this.isRefreshing = false;
    }
    /**
     * run WebSocket API listener
     */
    run() {
        this.isRestartScheduled = false;
        logger_1.default.debug(`Run ${this.marketName} prices listener`);
        this.ws = new WebSocket(this.url);
        this.ws.addEventListener('error', (err) => {
            if (this.isRestartScheduled) {
                return;
            }
            logger_1.default.debug(`websocket ${this.url} crashed: ` + err.message + ' ;Next start was scheduled');
            this.isRestartScheduled = true;
            try {
                this.ws.terminate();
            }
            catch (err) {
                return logger_1.default.warn(err.message);
            }
            setInterval(() => {
                this.run();
            }, 600000);
        });
        this.channels = new Map();
        this.ws.on('open', () => this.onSocketOpen());
        this.ws.on('message', async (data) => this.onSocketMessage(data));
    }
    /**
     * run WebSocket API listener (if it not already run), start's hooks and keepAlive process
     */
    start() {
        if (this.config[this.marketName].active === true) {
            this.saveFiatPrices()
                .then(() => {
                this.run();
                this.keepAliveProcess();
                this.onStart();
                this.updateFiatPricesProcess();
            })
                .catch(err => {
                logger_1.default.error(`Error while saving fiat prices:${err.message}`);
            });
        }
    }
    /**
     * sends data to Socket API if it falls retry in 10 seconds until it succeeds
     * @param json - data to send
     * @param cb - callback contains error
     */
    sendAnyWay(json, cb) {
        async.doWhilst((cb) => {
            this.ws.send(json, (error) => {
                if (!error) {
                    return cb(null, true);
                }
                if (_.includes(error.message, "WebSocket is not open")) {
                    logger_1.default.debug('targets websocket is not open');
                    cb(new Error("WebSocket is not open"));
                }
                setTimeout(() => {
                    cb(null, false);
                }, 10000);
            });
        }, (isResolved) => {
            return !isResolved;
        }, cb);
    }
    /**
     * calls when heartbeat message comes from server and update socket status
     */
    heartbeat() {
        // logger.debug(`${this.marketName} heartbeat`);
        this.isAlive = true;
    }
    /**
     * control every minute that connection is still alive. If it falls that
     * restarts socket connection;
     */
    keepAliveProcess() {
        const interval = setInterval(() => {
            if (this.isRestartScheduled) {
                this.isAlive = true;
                return;
            }
            if (this.isAlive === false)
                return this.restart('Connection is dead by keepAliveProcess');
            this.isAlive = false;
            this.ping();
        }, 60000);
    }
    /**
     * restarts connection and run market listener in 30 seconds
     * @param cause - restart cause
     */
    restart(cause) {
        if (!this.isRestartScheduled) {
            try {
                this.ws.terminate();
            }
            catch (err) {
                return logger_1.default.warn(err.message);
            }
            setTimeout(() => {
                this.run();
            }, 30000);
            logger_1.default.warn(`${this.marketName} websocket terminate; couse:${cause};
            Next start was scheduled`);
            this.isRestartScheduled = true;
        }
    }
    /**
     * parse message body
     * @param body - json-like message body to parse
     * @return parsed Object
     */
    bodyParse(body) {
        try {
            return JSON.parse(body);
        }
        catch (err) {
            throw new Error(err.toString() + '\n' + 'Body is:' + body);
        }
    }
    /**
     * get pair id by pair name
     * @param map - Map that store currency pairs
     * @param searchValue - pair name
     * @return pair id
     */
    getCreepyPairIdByName(map, searchValue) {
        for (let [key, value] of map.entries()) {
            if (value.name === searchValue)
                return key;
        }
    }
    /**
     * subscribe each cryptocurrency pair
     */
    subscribeAll(pairMap) {
        let values = Array.from(pairMap.values());
        async.series(values.map((el) => this.subscribePair.bind(this, el.name)), (err) => {
            if (err) {
                this.restart(err.message);
            }
        });
    }
}
exports.default = MarketViaWebSocket;
