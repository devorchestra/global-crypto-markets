"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const marketViaXML_1 = require("../abstract/marketViaXML");
class EuroMarket extends marketViaXML_1.default {
    constructor(config) {
        super(config);
        this.url = config.euro.url;
    }
    start() {
        return setInterval(() => {
            this.getData()
                .then(xml => {
                var regex = /<Cube currency='USD' rate='(.*?)'/;
                xml.replace(regex, ($0, rate) => {
                    rate = parseFloat(rate);
                    this.prices['EUR/USD'] = {
                        "exchange": "eurofxref",
                        "pair": 'EUR/USD',
                        "bid": rate - (rate / 100) * 0.2,
                        "ask": rate + (rate / 100) * 0.2,
                        "flow": "ticker",
                        'timeStamp': new Date().getTime()
                    };
                    this.prices['USD/EUR'] = this.invert(this.prices['EUR/USD']);
                    this.prices['USD/EUR'].timeStamp = this.prices['EUR/USD'].timeStamp;
                });
            });
        }, 2000); // 12h - 43200000
    }
}
exports.default = EuroMarket;
