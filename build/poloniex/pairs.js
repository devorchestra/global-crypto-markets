module.exports = [
    [24, { "name": "DASH/BTC" }],
    [50, { "name": "LTC/BTC" }],
    [148, { "name": "ETH/BTC" }],
    [201, { "name": "EOS/BTC" }],
    [202, { "name": "EOS/ETH" }],
    [196, { "name": "OMG/BTC" }],
    [197, { "name": "OMG/ETH" }],
    [204, { "name": "SNT/BTC" }],
    [205, { "name": "SNT/ETH" }],
    [121, { "name": "BTC/USD" }],
    [122, { "name": "DASH/USD" }],
    [123, { "name": "LTC/USD" }],
    [149, { "name": "ETH/USD" }],
    [203, { "name": "EOS/USD" }],
    [206, { "name": "SNT/USD" }]
];
