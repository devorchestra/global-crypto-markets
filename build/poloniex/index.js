"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
const logger_1 = require("../logger");
const marketViaWebSocket_1 = require("../abstract/marketViaWebSocket");
/**
 * @hidden
 */
const pairMap = new Map(require('./pairs'));
/**
 * @hidden
 */
const _ = require('lodash');
class Poloniex extends marketViaWebSocket_1.default {
    constructor(config) {
        super(config);
        this.url = config.poloniex.url;
        this.marketName = 'poloniex';
    }
    onStart() {
    }
    onSocketOpen() {
        return this.subscribeAll();
    }
    onSocketMessage(data) {
        let object;
        try {
            object = this.bodyParse(data);
        }
        catch (err) {
            logger_1.default.error('Poloniex websocket: corrupted message; Skipping.. ');
            return;
        }
        if (object[0] && object[0] == 1010) {
            return this.heartbeat();
        }
        if (object[0] && object[0] == 1002) {
            this.heartbeat();
            let message = object[2];
            if (!message)
                return;
            let pairId = message[0];
            let pair = pairMap.get(pairId);
            if (!pair)
                return;
            let BID = +message[3];
            if (!BID || typeof BID !== 'number')
                return;
            let ASK = +message[2];
            if (!ASK || typeof ASK !== 'number')
                return;
            const data = {
                "exchange": "poloniex",
                "pair": pair.name,
                "bid": BID,
                "ask": ASK,
                "flow": "ticker",
                'timeStamp': new Date().getTime()
            };
            return this.prices[pair.name] = data;
        }
    }
    ping() {
    }
    subscribeAll() {
        const substribleTicker = {
            "command": "subscribe",
            "channel": 1002
        };
        this.sendAnyWay(JSON.stringify(substribleTicker), (err) => {
            if (err) {
                this.restart(err.message);
            }
        });
    }
    /**
     * subscribe each cryptocurrency pair
     */
    subscribePair() {
    }
}
exports.default = Poloniex;
