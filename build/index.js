"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
const logger_1 = require("./logger");
const bitfinex_1 = require("./bitfinex");
const index_1 = require("./gdax/index");
const index_2 = require("./poloniex/index");
/**
 * @hidden
 */
const _ = require("lodash");
/**
 * @hidden
 */
const Promise = require("bluebird");
/**
 * @hidden
 */
const loadConfig = require('ini-config');
const index_3 = require("./postgresConnection/index");
/**
 * a wrapper that finds average price all over cryptocurrency markets
 */
class GlobalMarket {
    constructor() {
        this.exchangeConnectors = new Map();
    }
    /**
     * init each market observer
     */
    init() {
        return new Promise((resolve, reject) => {
            loadConfig('./config.ini', (err, config) => {
                if (err)
                    return reject(err);
                this.config = config;
                this.exchangeConnectors.set('bitfinex', new bitfinex_1.default(config));
                this.exchangeConnectors.set('gdax', new index_1.default(config));
                this.exchangeConnectors.set('poloniex', new index_2.default(config));
                this.exchangeConnectors.forEach(exchange => {
                    exchange.start();
                });
                logger_1.default.debug("GlobalMarket service initiated");
                resolve();
            });
        });
    }
    /**
     * get price validity time
     */
    getPriceValidityTime(req) {
        //TODO
        return 100000000;
    }
    /**
     * get exchange price
     * @param currency -  currency to exchange
     * @param base - base currency
     */
    getExchangePrice(currency, base) {
        currency = _.toUpper(currency);
        base = _.toUpper(base);
        if (base == currency) {
            return {
                "pair": currency + "/" + base,
                "bid": 1,
                "ask": 1,
                'timeStamp': new Date().getTime()
            };
        }
        if (currency == 'OPEX') {
            let opexPrice = {
                pair: `${currency}/${base}`,
                "bid": this.config.opex.bid,
                "ask": this.config.opex.ask,
                'timeStamp': new Date().getTime()
            };
            if (base == 'USD')
                return opexPrice;
            if (base == 'COI')
                return opexPrice;
            if (base == 'TMU')
                return opexPrice;
            let coinPrice = this.getAVGPrice(base, 'USD');
            return {
                "pair": currency + "/" + base,
                "bid": opexPrice.bid / coinPrice.ask,
                "ask": opexPrice.ask / coinPrice.bid,
                'timeStamp': new Date().getTime()
            };
        }
        if (currency == 'COI') {
            let coiPrise = {
                pair: `${currency}/${base}`,
                "bid": this.config.coi.bid,
                "ask": this.config.coi.ask,
                'timeStamp': new Date().getTime()
            };
            if (base == 'USD')
                return coiPrise;
            if (base == 'OPEX')
                return coiPrise;
            if (base == 'TMU')
                return coiPrise;
            let coinPrice = this.getAVGPrice(base, 'USD');
            return {
                "pair": currency + "/" + base,
                "bid": coiPrise.bid / coinPrice.ask,
                "ask": coiPrise.ask / coinPrice.bid,
                'timeStamp': new Date().getTime()
            };
        }
        if (currency == 'TMU') {
            let tmuPrise = {
                pair: `${currency}/${base}`,
                "bid": this.config.tmu.bid,
                "ask": this.config.tmu.ask,
                'timeStamp': new Date().getTime()
            };
            if (base == 'USD')
                return tmuPrise;
            if (base == 'OPEX')
                return tmuPrise;
            if (base == 'COI')
                return tmuPrise;
            let coinPrice = this.getAVGPrice(base, 'USD');
            return {
                "pair": currency + "/" + base,
                "bid": tmuPrise.bid / coinPrice.ask,
                "ask": tmuPrise.ask / coinPrice.bid,
                'timeStamp': new Date().getTime()
            };
        }
        if (base == 'OPEX') {
            return this.invertTokenPrice(currency, base);
        }
        if (base == 'COI') {
            return this.invertTokenPrice(currency, base);
        }
        if (base == 'TMU') {
            return this.invertTokenPrice(currency, base);
        }
        return this.getAVGPrice(currency, base);
    }
    /**
     * invert Token Price
     * @param currency -  currency to exchange
     * @param base - base currency
     */
    invertTokenPrice(currency, base) {
        let data = this.getExchangePrice(base, currency);
        let bid = data.bid;
        return {
            pair: `${currency}/${base}`,
            bid: 1 / data.ask,
            ask: 1 / bid
        };
    }
    /**
     * get average prise
     * @param currency -  currency to exchange
     * @param base - base currency
     */
    getAVGPrice(currency, base) {
        let avgPrice = {
            pair: currency + "/" + base,
            bid: 0,
            ask: 0,
            timeStamp: null
        };
        let count = 0;
        this.exchangeConnectors.forEach(exchange => {
            let price = exchange.getLastPrice(currency, base);
            if (price) {
                if (!avgPrice.timeStamp) {
                    avgPrice.bid += price.bid;
                    avgPrice.ask += price.ask;
                    avgPrice.timeStamp = price.timeStamp;
                    count++;
                }
                else {
                    if (!(avgPrice.timeStamp - price.timeStamp > 120000)) {
                        avgPrice.bid += price.bid;
                        avgPrice.ask += price.ask;
                        avgPrice.timeStamp = _.max([price.timeStamp, avgPrice.timeStamp]);
                        count++;
                    }
                }
            }
        });
        if (!count) {
            return {
                pair: currency + "/" + base,
                bid: NaN,
                ask: NaN,
                timeStamp: null
            };
        }
        avgPrice.bid = avgPrice.bid / count;
        avgPrice.ask = avgPrice.ask / count;
        return avgPrice;
    }
    /**
     * start process that saves prices to DB. ( every 10 seconds)
     * @param pgConfig  -  parsed Postgres config
     * @param currencies  - available currency array
     */
    initSavePriceProcess(pgConfig, currencies) {
        let db = new index_3.default(pgConfig);
        for (let i in currencies) {
            setInterval(() => {
                let price;
                if (currencies[i] == "OPEX") {
                    price = {
                        pair: currencies[i] + "/USD",
                        bid: this.config.opex.bid,
                        ask: this.config.opex.ask
                    };
                }
                else if (currencies[i] == "COI") {
                    price = {
                        pair: currencies[i] + "/USD",
                        bid: this.config.coi.bid,
                        ask: this.config.coi.ask
                    };
                }
                else {
                    price = this.getExchangePrice(currencies[i].toUpperCase(), "USD");
                }
                if (price && typeof price.bid == 'number' && !isNaN(price.bid)) {
                    db.insertPriceData(currencies[i].toUpperCase() + "USD", price.bid);
                }
            }, 10000);
        }
        logger_1.default.debug("GlobalMarket SavePriceProcess initiated");
    }
}
exports.default = GlobalMarket;
