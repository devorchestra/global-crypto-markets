"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let createLogger = require('devorchestra-logger');
let packageJson = require('../package.json');
let dottie = require('dottie');
let sentryOptions = {
    dsn: 'https://224e65c978a7415d8ae1b0e2b73644e1@sentry.io/1277185',
    level: 'error',
    tags: { service: "global-crypto-markets" }
};
let logLvl = dottie.get(process, 'env.LOG_LEVEL');
let logger = console;
if (logLvl) {
    logger = createLogger(packageJson.name, logLvl, sentryOptions);
}
exports.default = logger;
