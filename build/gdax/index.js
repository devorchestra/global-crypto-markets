"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @hidden
 */
const logger_1 = require("../logger");
const marketViaWebSocket_1 = require("../abstract/marketViaWebSocket");
/**
 * @hidden
 */
const pairMap = new Map(require('./pairs'));
/**
 * @hidden
 */
const _ = require('lodash');
/**
 * @hidden
 */
const async = require('async');
class Gdax extends marketViaWebSocket_1.default {
    constructor(config) {
        super(config);
        this.url = config.gdax.url;
        this.marketName = 'gdax';
    }
    onStart() {
    }
    onSocketOpen() {
        return this.subscribeAll(pairMap);
    }
    onSocketMessage(data) {
        let object;
        try {
            object = this.bodyParse(data);
        }
        catch (err) {
            logger_1.default.error('Gdax websocket: corrupted message; Skipping.. ');
            return;
        }
        if (object.type === 'error') {
            logger_1.default.warn('gdax error: ' + object.message);
            return;
        }
        if (object.type === 'heartbeat') {
            this.heartbeat();
            // let p = pairMap[object.product_id];
            //
            // if (!p) return;
            //
            // let price = this.prices[p.name];
            //
            // if (!price) return;
            //
            // price.timeStamp = new Date().getTime();
            return;
        }
        if (object.type === 'ticker') {
            this.heartbeat();
            const pairValue = pairMap.get(object.product_id);
            if (!pairValue)
                return;
            let pair = pairValue.name;
            let BID = +object.best_bid;
            let ASK = +object.best_ask;
            if (!BID || !ASK || typeof ASK !== 'number' || typeof ASK !== 'number')
                return;
            let data = {
                "exchange": "GDAX",
                "pair": pair,
                "bid": BID,
                "ask": ASK,
                "flow": "ticker",
                'timeStamp': new Date().getTime()
            };
            this.prices[pair] = data;
        }
    }
    ping() {
    }
    subscribePair(pair, cb) {
        let creepPair = this.getCreepyPairIdByName(pairMap, pair);
        let substribleRequest = {
            "type": "subscribe",
            "product_ids": [creepPair],
            "channels": [
                "ticker",
                "heartbeat",
            ]
        };
        this.sendAnyWay(JSON.stringify(substribleRequest), cb);
    }
}
exports.default = Gdax;
