# Overview
This module is responsible for getting cryptocurrencies prices (relative and "to USD") and cache prices to pg storage. It's related to crypto-exchanges-service and coin-services.

## Usage

add dependency to the package.json : "global-crypto-markets": "bitbucket:devorchestra/global-crypto-markets"

# Structure 

[diagram](https://doc-14-90-docs.googleusercontent.com/docs/securesc/dpab5refs30iov4ji52bv9tvardoo9lf/4ctd7lvjph2d8bod3cdfgp8o5cmqqv0f/1547128800000/13320056312508506686/13320056312508506686/1ryPq6WVpbDbadoOwBgqZV4TafokbMLJG?h=17227683681668449164&nonce=udfcr8qcobn6k&user=13320056312508506686&hash=6ajhcdl1jeg7bncmom4f7vin657ktcui)