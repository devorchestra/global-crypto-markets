/**
 * @hidden
 */
import logger from "../logger";

/**
 * @hidden
 */
const pg = require('pg');

/**
 * a wrapper on Postgres Connection
 */
class PostgresConnection {

    /**
     * a Postgres Connection
     */
    private pool;

    constructor(connectionConfig) {
        this.pool = new pg.Pool({
            database: connectionConfig.database,
            port: connectionConfig.port,
            nolimit: connectionConfig.nolimit,
            user: connectionConfig.user,
            password: connectionConfig.password,
            host: connectionConfig.host,
            ssl: connectionConfig.ssl
        });

        this.pool.on('error', (error, client) => {
            logger.debug('Unexpected postgres connection error' + JSON.stringify(error));
            // process.exit(1)
        });


    }

    /**
     * a method that allows save pair's price to Postgres Database
     * @param pair - pair's name
     * @param value - prise
     */
    insertPriceData(pair: string, value: number) {
        let query = `INSERT INTO ${pair.toLowerCase()} (price) VALUES ($1)`;
        return this.pool.query(query, [value])
            .catch((error) => {
                logger.debug('Postgres insert price data error' + JSON.stringify(error.message));
            });
    }
}


export default PostgresConnection