module.exports=[

    ["BTC-EUR",{"name":"BTC/EUR"}],
    ["BTC-GBP",{"name":"BTC/GBP"}],
    ["BTC-USD",{"name":"BTC/USD"}],

    ["ETH-BTC",{"name":"ETH/BTC"}],
    ["ETH-EUR",{"name":"ETH/EUR"}],
    ["ETH-USD",{"name":"ETH/USD"}],

    ["LTC-BTC",{"name":"LTC/BTC"}],
    ["LTC-EUR",{"name":"LTC/EUR"}],
    ["LTC-USD",{"name":"LTC/USD"}]
];