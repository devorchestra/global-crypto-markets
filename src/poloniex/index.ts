/**
 * @hidden
 */
import logger from "../logger";

import MarketViaWebSocket from '../abstract/marketViaWebSocket';

/**
 * @hidden
 */
const pairMap = new Map(require('./pairs'));

/**
 * @hidden
 */
const _ = require('lodash');

class Poloniex extends MarketViaWebSocket {

    constructor(config) {
        super(config);
        this.url = config.poloniex.url;
        this.marketName = 'poloniex';
    }

    protected onStart() {
    }

    protected onSocketOpen() {
        return this.subscribeAll();
    }

    protected onSocketMessage(data: any) {
        let object: any;

        try {
            object = this.bodyParse(data);
        } catch (err) {
            logger.error('Poloniex websocket: corrupted message; Skipping.. ');
            return;
        }

        if (object[0] && object[0] == 1010) {
            return this.heartbeat();
        }


        if (object[0] && object[0] == 1002) {
            this.heartbeat();

            let message = object[2];

            if (!message) return;

            let pairId = message[0];

            let pair: any = pairMap.get(pairId);

            if (!pair) return;

            let BID = +message[3];

            if (!BID || typeof BID !== 'number') return;

            let ASK = +message[2];

            if (!ASK || typeof ASK !== 'number') return;

            const data = {
                "exchange": "poloniex",
                "pair": pair.name,
                "bid": BID,
                "ask": ASK,
                "flow": "ticker",
                'timeStamp': new Date().getTime()
            };

            return this.prices[pair.name] = data;

        }
    }

    protected ping() {
    }

    protected subscribeAll() {

        const substribleTicker = {
            "command": "subscribe",
            "channel": 1002
        };

        this.sendAnyWay(JSON.stringify(substribleTicker), (err) => {

            if (err) {
                this.restart(err.message);
            }
        });
    }

    /**
     * subscribe each cryptocurrency pair
     */
    protected subscribePair() {
    }
}

export default Poloniex;