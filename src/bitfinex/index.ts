/**
 * @hidden
 */
import logger from "../logger";

import MarketViaWebSocket from '../abstract/marketViaWebSocket';

/**
 * @hidden
 */
const pairMap = new Map(require('./pairs'));

/**
 * @hidden
 */
const _ = require('lodash');

class Bitfinex extends MarketViaWebSocket {

    constructor(config) {
        super(config);
        this.isRefreshing = false;
        this.url = config.bitfinex.url;
        this.marketName = 'bitfinex';
        this.channels = {};

    }

    protected onStart() {}

    protected onSocketOpen() {
        return this.subscribeAll(pairMap);
    }

    protected onSocketMessage(data) {
        let object: any;

        try {
            object = this.bodyParse(data);
        }
        catch (err) {
            logger.error('Bitfinex websocket: corrupted message; Skipping.. ');
            return;
        }

        if (object.event === 'ping') {
            return this.ws.send(JSON.stringify({
                "event": "pong"
            }),(err)=>{
                if(err)
                logger.debug(err.message)
            });
        }

        if (object.event === 'pong') {
            return this.heartbeat();
        }

        if (object.event === 'info') {

            this.heartbeat();

            if (object.code === '20051') {
                logger.warn('info message 20051');
                this.channels = {};
                return this.restart('20051 : Stop/Restart Websocket Server (please try to reconnect)');
            }

            if (object.code === '20060') {
                // 20060 : Refreshing data from the Trading Engine.
                // Please pause any activity and resume after receiving the
                // info message 20061 (it should take 10 seconds at most).
                logger.warn('info message 20060');
                this.isRefreshing = true;
            }

            if (object.code === '20061') {
                // 20061 : Done Refreshing data from the Trading Engine.
                // You can resume normal activity. It is advised to unsubscribe/subscribe again all channels.
                logger.warn('info message 20061');
                this.isRefreshing = false;
                return this.subscribeAll(pairMap);
            }
        }

        if (this.isRefreshing) {
            return;
        }

        if (object.event === 'subscribed') {
            this.heartbeat();

            let pair: any = pairMap.get(object.pair);

            if (!pair) return;

            if (object.channel === 'ticker' || object.channel === 'trades') {
                this.channels[object.chanId] = pair.name
            }

            return;
        }

        if (object.event === 'error') {
            logger.warn('Bitfinex error: ' + JSON.stringify(object));

            // if (object.code === 10300) {
            //     logger.warn(`retry for ${object.pair} by code 10300`);
            //     return this.subscribePair(object.pair);
            // }

            return
        }

        if (object[0] && object[1] == 'hb') {
            this.heartbeat();
            // let pair = this.channels[object[0]];
            // let price = this.prices[pair];
            // if (!price) return;
            // price.timeStamp = new Date().getTime();
            return;
        }

        if (object.length && object.length > 3) {

            this.heartbeat();

            let pair = this.channels[object[0]];

            if (!pair) return;

            const BID = object[1];

            if (!BID || typeof BID !== 'number') return;

            const ASK = object[3];

            if (!ASK || typeof ASK !== 'number') return;

            const data = {
                "exchange": "BITFINEX",
                "pair": pair,
                "bid": +BID,
                "ask": +ASK,
                "flow": "ticker",
                'timeStamp': new Date().getTime()
            };

            return this.prices[data.pair] = data;

        }
    }

    protected ping() {
        this.ws.send(JSON.stringify({
            "event": "ping",
        }),(err)=>{
            if(err)
                logger.debug(err.message)
        });
    }

    protected subscribePair(pair,cb) {

        let creepPair = this.getCreepyPairIdByName(pairMap, pair);

        const substribleTicker = {
            "event": "subscribe",
            "channel": "ticker",
            "pair": creepPair
        };


        this.sendAnyWay(JSON.stringify(substribleTicker), cb);

    }
}

export default Bitfinex;