/**
 * @hidden
 */
import logger from "../logger";

/**
 * @hidden
 */
import _ = require('lodash');

/**
 * @hidden
 */
const rp = require('request-promise');

abstract class Market {

    constructor(config) {
        this.prices = {};
        this.url_euro = config.euro.url;
    }


    url_euro: string;

    /**
     * Object to save currency pair name and market price.
     */
    prices: any;

    /**
     * market name
     */
    marketName: string;

    /**
     * gets the most recent price from local in-memory Map;
     * currency BTC and base USD gives BTC to USD exchange price
     * @param currency -  currency to exchange
     * @param base - base currency
     */
    public getLastPrice(currency: string, base: string) {

        function getPriceThruSomething(currencyOnSomething, somethingOnBase) {
            let data = _.clone(currencyOnSomething);
            let data1 = _.clone(somethingOnBase);
            let prm1 = data.pair.split('/');
            let prm2 = data1.pair.split('/');
            let result: any = {
                timeStamp: _.max([data.timeStamp, data1.timeStamp])
            };
            result.pair = prm1[0] + '/' + prm2[0];
            result.bid = data.bid / data1.ask;
            result.ask = data.ask / data1.bid;
            return result
        }

        if (this.prices[currency + '/' + base]) {
            logger.debug(`${this.marketName}:${currency + '/' + base}===>${this.prices[currency + '/' + base].bid}`);
            return this.prices[currency + '/' + base];
        }

        if (this.prices[base + '/' + currency]) {
            let invertPair = this.invert(this.prices[base + '/' + currency]);
            logger.debug(`${this.marketName}:*${invertPair.pair}===>${invertPair.bid}`);
            return invertPair;
        }

        // region depends on 'EUR'

        if (base == 'EUR') {
            let usdBase = this.getLastPrice(currency, 'USD');
            if (this.prices['EUR/USD'] && usdBase) {
                return getPriceThruSomething(usdBase, this.prices['EUR/USD'])
            }

        }

        if (currency == 'EUR') {
            let inverted = this.getLastPrice(base, currency);
            if (inverted)
                return this.invert(inverted);

        }

        //endregion depends on 'EUR'

        // region depends on 'BTC'

        if (currency == 'BTC' || base == 'BTC') {
            return null;
        }

        if (this.prices[currency + '/BTC'] && this.prices[base + '/BTC']) {
            let res = getPriceThruSomething(this.prices[currency + '/BTC'], this.prices[base + '/BTC']);
            logger.debug(`${this.marketName}:**${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices['BTC/' + currency] && this.prices['BTC/' + base]) {
            let res = getPriceThruSomething(
                this.invert(this.prices['BTC/' + currency]),
                this.invert(this.prices['BTC/' + base])
            );
            logger.debug(`${this.marketName}***${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices[currency + '/BTC'] && this.prices['BTC/' + base]) {
            let res = getPriceThruSomething(
                this.prices[currency + '/BTC'],
                this.invert(this.prices['BTC/' + base])
            );
            logger.debug(`${this.marketName}****${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices['BTC/' + currency] && this.prices[base + '/BTC']) {
            let res = getPriceThruSomething(
                this.invert(this.prices['BTC/' + currency]),
                this.prices['BTC/' + base]
            );
            logger.debug(`${this.marketName}***${res.pair}===>${res.bid}`);
            return res;
        }

        // endregion depends on 'BTC'

        // region depends on 'ETH'

        if (currency == 'ETH' || base == 'ETH') {
            return null;
        }

        if (this.prices[currency + '/ETH'] && this.prices[base + '/ETH']) {
            let res = getPriceThruSomething(this.prices[currency + '/ETH'], this.prices[base + '/ETH']);
            logger.debug(`${this.marketName}:**${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices['ETH/' + currency] && this.prices['ETH/' + base]) {
            let res = getPriceThruSomething(
                this.invert(this.prices['ETH/' + currency]),
                this.invert(this.prices['ETH/' + base])
            );
            logger.debug(`${this.marketName}***${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices[currency + '/ETH'] && this.prices['ETH/' + base]) {
            let res = getPriceThruSomething(
                this.prices[currency + '/ETH'],
                this.invert(this.prices['ETH/' + base])
            );
            logger.debug(`${this.marketName}****${res.pair}===>${res.bid}`);
            return res;
        }

        if (this.prices['ETH/' + currency] && this.prices[base + '/ETH']) {
            let res = getPriceThruSomething(
                this.invert(this.prices['ETH/' + currency]),
                this.prices['ETH/' + base]
            );
            logger.debug(`${this.marketName}***${res.pair}===>${res.bid}`);
            return res;
        }

        // endregion  depends on 'ETH'

        return null;
    }

    protected invert(price) {
        let data = _.clone(price);
        let prm = data.pair.split('/');
        let result: any = {
            timeStamp: data.timeStamp || 1993
        };
        result.pair = `${prm[1]}/${prm[0]}`;
        let bid = data.bid;
        result.bid = 1 / data.ask;
        result.ask = 1 / bid;
        return result;
    }

    updateFiatPricesProcess() {
        return setInterval(() => {
            this.saveFiatPrices()
                .catch(err => {
                    logger.error(`Error while updating fiat prices:${err.message}`)
                });

        }, 43200000) // 12h
    }

    saveFiatPrices() {
        return rp(this.url_euro)
            .then(xml => {
                var regex = /<Cube currency='USD' rate='(.*?)'/;
                xml.replace(regex, ($0, rate) => {
                    rate = parseFloat(rate);
                    this.prices['EUR/USD'] = {
                        "exchange": "eurofxref",
                        "pair": 'EUR/USD',
                        "bid": rate - (rate / 100) * 0.2,
                        "ask": rate + (rate / 100) * 0.2,
                        "flow": "ticker",
                        'timeStamp': new Date().getTime()
                    };
                    this.prices['USD/EUR'] = this.invert(this.prices['EUR/USD']);
                    this.prices['USD/EUR'].timeStamp = this.prices['EUR/USD'].timeStamp;
                });
            })
    }
}

export default Market;