/**
 * @hidden
 */
import WebSocket = require('ws');

/**
 * @hidden
 */
import logger from "../logger";

/**
 * @hidden
 */
import async = require('async');

/**
 * @hidden
 */
import _ = require('lodash');

import Market from './market'

/**
 * This abstract class is responsible for describing
 * global cryptocurrency market that supports WebSocket API.
 */

abstract class MarketViaWebSocket extends Market {
    /**
     * URL that expose WebSockets API.
     * Ex: wss://api.foobar.com/ws/
     */
    url: string;

    /**
     * parsed config object
     */
    config: any;

    /**
     * WebSocket instance
     */
    ws: any;

    /**
     *  a flag showing that WebSocket instance was destroyed and next reconnect is on go
     */
    isRestartScheduled: boolean;

    /**
     * a flag showing that socket is Alive by heartbeat
     */
    isAlive: boolean;

    /**
     *  a flag showing that socket is on a refreshing process
     *  (waiting for state message from a server)
     */
    isRefreshing: boolean;

    /**
     * a map that store observing currency pairs
     */
    channels: any;


    /**
     * @param config parsed config object
     */
    constructor(config) {
        super(config);
        this.config = config;
        this.isRestartScheduled = false;
        this.isAlive = true;
        this.isRefreshing = false;
    }

    /**
     * run WebSocket API listener
     */

    protected run() {

        this.isRestartScheduled = false;
        logger.debug(`Run ${this.marketName} prices listener`);
        this.ws = new WebSocket(this.url);

        this.ws.addEventListener('error', (err) => {

            if(this.isRestartScheduled){
                return;
            }

            logger.debug(`websocket ${this.url} crashed: ` + err.message + ' ;Next start was scheduled');
            this.isRestartScheduled = true;

            try {
                this.ws.terminate();
            } catch (err) {
                return logger.warn(err.message);
            }

            setInterval(() => {
                this.run();
            }, 600000);

        });

        this.channels = new Map();

        this.ws.on('open', () => this.onSocketOpen());

        this.ws.on('message', async (data) => this.onSocketMessage(data));
    }

    /**
     * run WebSocket API listener (if it not already run), start's hooks and keepAlive process
     */
    public start() {
        if (this.config[this.marketName].active === true) {
            this.saveFiatPrices()
                .then(() => {
                    this.run();
                    this.keepAliveProcess();
                    this.onStart();
                    this.updateFiatPricesProcess();
                })
                .catch(err => {
                    logger.error(`Error while saving fiat prices:${err.message}`)
                });
        }
    }

    /**
     * onStart hook
     */
    protected abstract onStart()

    /**
     * onSocketOpen hook
     */
    protected abstract onSocketOpen()

    /**
     * onSocketMessage hook
     * @param data - data that comes to socket
     */
    protected abstract onSocketMessage(data: any)

    /**
     * send a standard ping message to socket API
     */
    protected abstract ping()

    /**
     * subscribe cryptocurrency pair to listen to price updates
     * @param pair - pair name
     * @param cb with error
     */
    protected abstract subscribePair(pair: string, cb: Function)

    /**
     * sends data to Socket API if it falls retry in 10 seconds until it succeeds
     * @param json - data to send
     * @param cb - callback contains error
     */
    protected sendAnyWay(json, cb) {
        async.doWhilst((cb) => {
            this.ws.send(json, (error) => {

                if (!error) {
                    return cb(null, true);
                }

                if (_.includes(error.message, "WebSocket is not open")) {
                    logger.debug('targets websocket is not open');
                    cb(new Error("WebSocket is not open"))
                }

                setTimeout(() => {
                    cb(null, false);
                }, 10000);

            });
        }, (isResolved: boolean) => {
            return !isResolved;
        }, cb);
    }

    /**
     * calls when heartbeat message comes from server and update socket status
     */
    protected heartbeat() {
        // logger.debug(`${this.marketName} heartbeat`);
        this.isAlive = true;
    }

    /**
     * control every minute that connection is still alive. If it falls that
     * restarts socket connection;
     */
    protected keepAliveProcess() {
        const interval = setInterval(() => {

            if (this.isRestartScheduled) {
                this.isAlive = true;
                return;
            }

            if (this.isAlive === false) return this.restart('Connection is dead by keepAliveProcess');

            this.isAlive = false;

            this.ping()

        }, 60000);
    }

    /**
     * restarts connection and run market listener in 30 seconds
     * @param cause - restart cause
     */
    protected restart(cause) {

        if (!this.isRestartScheduled) {

            try {
                this.ws.terminate();
            } catch (err) {
                return logger.warn(err.message);
            }

            setTimeout(() => {
                this.run()
            }, 30000);

            logger.warn(`${this.marketName} websocket terminate; couse:${cause};
            Next start was scheduled`);
            this.isRestartScheduled = true;
        }

    }

    /**
     * parse message body
     * @param body - json-like message body to parse
     * @return parsed Object
     */
    protected bodyParse(body) {
        try {
            return JSON.parse(body);
        } catch (err) {
            throw new Error(err.toString() + '\n' + 'Body is:' + body);
        }
    }

    /**
     * get pair id by pair name
     * @param map - Map that store currency pairs
     * @param searchValue - pair name
     * @return pair id
     */
    protected getCreepyPairIdByName(map, searchValue) {
        for (let [key, value] of map.entries()) {
            if (value.name === searchValue)
                return key;
        }
    }

    /**
     * subscribe each cryptocurrency pair
     */
    protected subscribeAll(pairMap) {

        let values = Array.from(pairMap.values());

        async.series(values.map((el: any) => this.subscribePair.bind(this, el.name)),
            (err) => {

                if (err) {
                    this.restart(err.message);
                }

            });
    }

}

export default MarketViaWebSocket;
